#!/usr/bin/env sh

set -e

MAX_SCAN_DURATION_SECONDS=1
REPORT="gl-dependency-scanning-report.json"

apk add coreutils jq
s=`cat $REPORT | jq -r '.scan.start_time' | date +%s -f -`
e=`cat $REPORT | jq -r '.scan.end_time' | date +%s -f -`
set +e
d=`expr $e - $s`
set -e
[ $DEBUG ] && echo "s: $s, e: $e, d: $d"
echo "Scan duration: $d(s)"
[ $d -gt $MAX_SCAN_DURATION_SECONDS ] && echo "Max scan duration ($MAX_SCAN_DURATION_SECONDS(s)) exceeded" && exit 1
echo 0